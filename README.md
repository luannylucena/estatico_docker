# Deploy de Site Estático com Docker, Docker Hub, GitLab CI/CD e AWS

## Descrição

Este projeto demonstra como configurar e implantar um site estático utilizando Docker, Docker Hub, GitLab CI/CD e AWS. Inclui arquivos de configuração para Terraform, scripts de deployment para Docker e configuração de pipeline no GitLab CI/CD.

## Índice

- [Instalação](#instalação)
- [Configuração](#configuração)
- [Uso](#uso)
- [Arquivos Incluídos](#arquivos-incluídos)
- [Contribuição](#contribuição)
- [Licença](#licença)

## Instalação

### Pré-requisitos

- Docker
- GitLab Runner
- Terraform
- AWS CLI

### Passos

1. Clone o repositório:
    ```bash
    git clone https://gitlab.com/usuario/projeto.git
    cd projeto
    ```

2. Configure o Docker e o GitLab Runner:
    ```bash
    ./script.sh
    ```

3. Configure o Terraform para criar os recursos na AWS:
    ```bash
    terraform init
    terraform apply
    ```

## Configuração

### GitLab CI/CD

No GitLab, adicione as variáveis de ambiente necessárias:

- `user`: Usuário do Docker Hub
- `password`: Senha do Docker Hub

Configure o pipeline do GitLab com o conteúdo em `.gitlab-ci.yml`.


## Uso

Execute o pipeline no GitLab para construir as imagens Docker e implantá-las no servidor AWS.

Verifique os containers rodando no servidor:
 - `docker ps`

## Arquivos Incluídos

 - `.gitlab-ci.yml`: Pipeline de CI/CD do GitLab.
 - `Dockerfile`: Arquivo de configuração para a construção da imagem Docker.
 - `script.sh`: Script de instalação de dependências.
 - `terraform/`: Configurações do Terraform para criar recursos na AWS.

Terraform Configuration
 - `main.tf`: Contém a definição da instância EC2 e dos grupos de segurança.
 - `variables.tf`: Definições de variáveis para configurar o projeto.

